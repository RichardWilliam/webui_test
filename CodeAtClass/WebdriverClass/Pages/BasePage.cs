﻿using OpenQA.Selenium;
using System;

namespace WebUIHomework_d2erx7
{
    public class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }

    }
}
