﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebUIHomework_d2erx7;

namespace WebdriverClass.Pages
{
    class HardverAproPage : BasePage
    {
        IWebElement hardwareSpan => Driver.FindElement(By.ClassName("icon-videocard"));
        public HardverAproPage(IWebDriver webDriver) : base(webDriver)
        {
            Driver.Url = "https://hardverapro.hu/index.html";
        }

        
    }
}
