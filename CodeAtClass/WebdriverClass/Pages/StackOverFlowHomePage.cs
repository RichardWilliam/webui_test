﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WebdriverClass.Pages;

namespace WebUIHomework_d2erx7.Pages
{
    class StackOverFlowHomePage : BasePage
    {
        private IWebElement loginBtn => Driver.FindElement(By.ClassName("s-btn__filled"));
        private IWebElement lftSideToggleBar => Driver.FindElement(By.ClassName("left-sidebar-toggle"));


        public StackOverFlowHomePage(IWebDriver webDriver) : base(webDriver)
        {
            webDriver.Url = "https://stackoverflow.com/";
        }


        public StackOFLoginWidget Login()
        {
            loginBtn.Click();
            return new StackOFLoginWidget(Driver);
        }

        public void createScreenshoot(string filename)
        {
            var screenShot = ((ITakesScreenshot)Driver).GetScreenshot();

            string dir = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory);

            screenShot.SaveAsFile(dir + filename + ".png", ScreenshotImageFormat.Png);
        }

        public StackOverFlowUserPage GetUsersPage()
        {
            lftSideToggleBar.Click();
            Driver.FindElement(By.XPath("/html/body/header/div/div[1]/div/div/div[1]/nav/ol/li[2]/ol/li[4]/a/div")).Click();
            return new StackOverFlowUserPage(Driver);
        }
    }
}
