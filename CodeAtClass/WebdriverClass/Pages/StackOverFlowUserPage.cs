﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebUIHomework_d2erx7;

namespace WebdriverClass.Pages
{
    class StackOverFlowUserPage : BasePage
    {
        IWebElement grid => Driver.FindElement(By.Id("user-browser"));
        public StackOverFlowUserPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public bool TitleAndUrlMatches(string url, string title)
        {
            return Driver.Url == url && Driver.Title == title;
        }
    }
}
