﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;
using WebUIHomework_d2erx7.Pages;

namespace WebUIHomework_d2erx7
{
    [TestFixture]
    class BeadandoPageObjectTest : TestBase
    {
        [Test]
        public void WrongCredentialsLoginTest()
        {
            StackOverFlowHomePage stackOFPage = new StackOverFlowHomePage(Driver);
            var widget = stackOFPage.Login();

            widget.Login("admin", "admin");
            Assert.That(widget.IsWrongUnamOrPassDivDisplayed());
        }

        [Test]
        public void TakeAscreenShot()
        {
            StackOverFlowHomePage stackOFPage = new StackOverFlowHomePage(Driver);
            var widget = stackOFPage.Login();
            try
            {
                widget.Login("admin", "admin");
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementExists(By.Id("NULL")));
            }
            catch (Exception)
            { 
                stackOFPage.createScreenshoot("screenshot_" + DateTime.Now.Ticks);
            }
        }

        [Test]
        [TestCase(new Object[] {"https://stackoverflow.com/users", "Users - Stack Overflow"})]
        public void PageTitleAndUrlChecl(string url, string title)
        {
            StackOverFlowHomePage homePage = new StackOverFlowHomePage(Driver);
            var result = homePage.GetUsersPage();

            Assert.That(result.TitleAndUrlMatches(url, title));
        }


    }
}
