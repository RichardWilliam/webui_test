﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebUIHomework_d2erx7.Pages
{
    class StackOFLoginWidget : BasePage
    {
        public StackOFLoginWidget(IWebDriver webDriver) : base(webDriver)
        {
        }

        public bool IsWrongUnamOrPassDivDisplayed()
        {
            return !Driver.FindElement(By.ClassName("js-error-message")).Displayed;
        }

        public void Login(string username, string password)
        {
            Driver.FindElement(By.Id("email")).SendKeys(username);
            Driver.FindElement(By.Id("password")).SendKeys(password);
            Driver.FindElement(By.Id("submit-button")).Click();
        }
    }
}
